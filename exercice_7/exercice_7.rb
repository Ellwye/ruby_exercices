class Coded
  def initialize(alphabet_corresp, phrase)
    @alphabet_corresp = alphabet_corresp
    @phrase = phrase
  end

  def phrase_code
    phrase_array = @phrase.split('')
    phrase_coded = []
    phrase_array.each_with_index do |letter, key|
      if @alphabet_corresp[0].include?(letter)
        key_of_letter = @alphabet_corresp[0].index(letter)
        letter_coded = @alphabet_corresp[1][key_of_letter]
        phrase_coded.push(letter_coded)
      else
        phrase_coded.push(letter)
      end
    end
    phrase_coded.join('')
  end
end

# Array correspondance lettres et symbole
alphabet_corresp = [
  ['a', 'c', 'd', 'e', 'g', 'i', 'l', 'n', 'o', 'r', 's'],
  ['4', '(', ')', '3', '6', '!', '1', '^', '0', '2', '$']
]

# L'utilisateur entre une phrase :
puts 'Saisissez une phrase :'
phrase = gets.chomp
phrase_code = Coded.new(alphabet_corresp, phrase)
puts phrase_code.phrase_code
