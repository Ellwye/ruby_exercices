#Exercice 6 :
# Construire un pendu.
# Les mots à trouver seront de 20, il devront être maximum de 8 caractères et
# pour le moment inscrit dans le code.
# Le joueur a le droit de jouer de façon illimitée.
# Maximum 6 essai. Si non c’est perdu.
# Si le joueur trouve le mot, on lui affichera le nombre d’essai qu’il lui a fallu pour
# trouver le mot du pendu.
class Pendu
  def initialize(liste_mots)
    @liste_mots = liste_mots
  end

  def mot_a_trouver
    key_word = rand(20)
    @word = @liste_mots[key_word].downcase.split('')
  end

  def lettre_saisie
    puts 'Saisissez une lettre :'
    lettre = gets.chomp
    until /^[a-zA-Z]{1}$/.match(lettre)
      puts 'Saisissez une seule lettre :'
      lettre = gets.chomp
    end
    lettre
  end

  def word_with_underscore
    word_array = @word
    word_underscores = []
    i = 0
    while i < word_array.length
      word_underscores.push('_')
      i += 1
    end
    word_underscores
  end

  def letter_in_word?(word_underscores, try, array_letters_try)
    lettre = lettre_saisie
    word_array = @word
    array_letters_try.push(lettre)
    word_array.each_with_index do |letter, key|
      word_underscores[key] = letter if letter == lettre
    end
    print "#{word_underscores.join(' ')} \n\n"
    try += 1 if word_array.include?(lettre) == false
    number_of_try(word_underscores, try, array_letters_try)
  end

  def number_of_try(word_underscores, try, array_letters_try)
    if try > 6
      "Game over ! \n Le mot à trouver était : #{@word.join}"
    elsif word_underscores.include?('_') == false
      'Bravo ! Tu as gagné.'
    else
      puts "Nombre d'erreurs : #{try}\n"
      print "Lettres tentées : #{array_letters_try.join(' - ')}\n\n\n\n"
      letter_in_word?(word_underscores, try, array_letters_try)
    end
  end
end

liste_mots = ['Avion', 'Arbalete', 'Banquise', 'Batterie', 'Brocante', 'Cloporte', 'Diapason', 'Gangster', 'Gothique', 'Herisson', 'Lave', 'Objectif', 'Paranoia', 'Passion', 'Quetsche', 'Scorpion', 'Symptome', 'Tour', 'Train', 'Utopique']

pendu = Pendu.new(liste_mots)
pendu.mot_a_trouver
mot_underscores = pendu.word_with_underscore
puts "Mot à trouver : #{mot_underscores.join(' ')} \n Attention ! Vous n'avez le droit qu'à 6 erreurs ! \n\n"

word_with_underscore_maj = pendu.letter_in_word?(mot_underscores, 0, [])
print "#{word_with_underscore_maj} \n\n"
