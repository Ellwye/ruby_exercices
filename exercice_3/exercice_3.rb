# Create array with numbers : 1 to 100
array = Array(1..100)
# Create new array with only even values
array_even = []
array.select do |v|
  array_even.push(v) if v.even?
end
print array_even
