# Def for name and age
def first_name
  puts 'What is yout first name ?'
  first_name = gets.chomp
  until /^[a-zA-Z]+$/.match(first_name) do
    puts 'Use only letters.'
    first_name = gets.chomp
  end
  first_name.capitalize!
end

def age
  puts 'How old are you ?'
  age = gets.chomp
  age = age.to_i if /^[0-9]{1,2}$/.match(age)
end

# Call defs first_name and age
first_name = first_name()
age = age()

# Puts first name, and if age isn't nil, puts age too
puts "My name is #{first_name}" + (age.nil? ? '' : ", I have #{age} years old.")
