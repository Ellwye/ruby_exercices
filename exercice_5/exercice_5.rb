#Exercice 5 :
# À la saisie de votre nom prénom, calculer le poids ramené sur un nombre de 1
# chiffre en prenant en compte l’emplacement de la lettre dans l’alphabet.

class NameMass
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
  end

  def split_in_array
    @first_name.split('') + @last_name.split('')
  end

  def associate_letters_to_numbers
    # Alphabet
    alphabet = Array('a'..'z')
    array_numbers = []
    alphabet.each_with_index do |letter, key|
      split_in_array.each do |l|
        array_numbers.push(key + 1) if l == letter
      end
    end
    array_numbers
  end

  def addition
    addition_numbers = associate_letters_to_numbers.map(&:to_i).inject(0, :+)
    while (addition_numbers > 9)
      addition_numbers = addition_numbers.to_s.split('')
      addition_numbers = addition_numbers.map(&:to_i).sum
    end
    addition_numbers
  end
end

def first_name
  puts 'What is yout first name ?'
  first_name = gets.chomp
  until /^[a-zA-Z]+$/.match(first_name) do
    puts 'Use only letters.'
    first_name = gets.chomp
  end
  first_name = first_name.downcase
end

def last_name
  puts 'What is yout last name ?'
  last_name = gets.chomp
  until /^[a-zA-Z]+$/.match(last_name) do
    puts 'Use only letters.'
    last_name = gets.chomp
  end
  last_name = last_name.downcase
end

# Call defs first_name and last_name
first_name = first_name()
last_name = last_name()

puts "First name : #{first_name}
Last name : #{last_name}"

mass = NameMass.new(first_name, last_name)
# print mass.split_in_array
# print mass.associate_letters_to_numbers
puts "The mass of your name is #{mass.addition}"
