tp_ruby

Explications
#Dans le cadre du télétravail, il vous est demandé d’effectuer les exercices sui-vants en ruby
Les consignes sont :
•Création d’un dépôt GIT sur framagit en public.
•Chaque exercice sera compris dans un répertoire portant le nom de l’exercice.
•Les commits seront pertinents suivant l’exercice à effectuer.
•Les codes devront être commentés afin de comprendre ce qu’il fait et à quoi il sert.

#Exercice_1 :  ( ça c’est le nom de l’exercice :) )
Création d’un tableau avec les chiffres de 1 à 100.  (2 méthodes possibles)
Une fois créer, afficher chaque valeur.

#Exercice_2:    ( c’est toujours le nom de l’exercice :)  si si )
Depuis le tableau de l’exercice 1, méthode simplifiée,  afficher les nombres compris entre 30 et 59.

#Exercice 3:   (je pense que là c’est compris :-D )
Toujours depuis l’exercice_1  Créer un nouveau tableau avec les nombre paires du tableau de l’exercice_1 en utilisant la méthode «select»

#Exercice 4 :  (vous êtes sûr ? ) :)
Ecrire une méthode qui prendra des paramètres ‘first_name’ et ‘age’A l’appel de la méthode, celle ci renverra : «Je m’apelle ‘first_name’ , j’ai ‘age’ans » Si age n’est pas précisé la chaine renvoi «Je m’apelle ‘first_name’ »

#Exercice 5 :
À la saisie de votre nom prénom, calculer le poids ramené sur un nombre de 1
chiffre en prenant en compte l’emplacement de la lettre dans l’alphabet.

#Exercice 6 :
Construire un pendu.
Les mots à trouver seront de 20, il devront être maximum de 8 caractères et
pour le moment inscrit dans le code.
Le joueur a le droit de jouer de façon illimitée.
Maximum 6 essai. Si non c’est perdu.
Si le joueur trouve le mot, on lui affichera le nombre d’essai qu’il lui a fallu pour
trouver le mot du pendu.

#Exercice 7 :
Depuis la saisie d'un utilisateur, convertir la saisie pour en faire un "mot de passe" comme pour les mdp de wifi.
Exemple :  "roland moreno" deivent R0l4nd_M0r3n0"
https://fr.wikipedia.org/wiki/Leet_speak
Sur la page wiki vous trouverez toutes les lettres qui ont une correspondance en chiffre